
package py.una.pol.cliente.main;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import py.una.pol.cliente.metodos.MetodoGet;
import py.una.pol.cliente.model.Persona;
import py.una.pol.cliente.metodos.MetodoPost;
import py.una.pol.cliente.model.Asignatura;
import py.una.pol.cliente.model.Persona_Asignatura;

public class MenuCliente {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        BufferedReader inFromUser
                = new BufferedReader(new InputStreamReader(System.in));
        Integer opcion;
        List<Asignatura> asignatura;
        List<Persona> persona;
        String strcodigo;
        String strcedula;
        Persona p;
        Asignatura asig;

        boolean salir = false;
        do {
            try {

                System.out.println("====================================================");
                System.out.println("Opciones:");
                System.out.println("1 Crear Persona");
                System.out.println("2 Listar Personas");
                System.out.println("3 Crear Asignatura");
                System.out.println("4 Listar Asignatura");
                System.out.println("5 Asociar Persona con Asignatura");
                System.out.println("6 Listar Asignaturas asociadas a una Persona");
                System.out.println("7 Listar Personas asociadas a una Asignatura");
                System.out.println("8 Salir");
                System.out.println("Introduzca el numero de la opcion:");
                opcion = sc.nextInt();
                switch (opcion) {
                    case 1:
                        /* Crear persona ingresando numero de cedula, nombre y apellido */
                        System.out.print("Ingrese el número de cédula (debe ser numérico): ");
                        strcedula = inFromUser.readLine();
                        Long cedula = 0L;
                        try {
                            cedula = Long.parseLong(strcedula);
                        } catch (Exception e1) {

                        }
                        System.out.print("Ingrese el nombre: ");
                        String nombre = inFromUser.readLine();
                        System.out.print("Ingrese el apellido: ");
                        String apellido = inFromUser.readLine();

                        p = new Persona(cedula, nombre, apellido);
                        MetodoPost.crearPersonas(p);
                        break;

                    case 2:
                        /* Listar personas existentes en la base de datos */
                        persona = MetodoGet.listarPersonas();
                        if (persona == null) {
                            System.out.println("Lista de Personas vacia");
                        } else {
                            MostrarPersonas(persona);

                        }
                        break;
                    case 3:

                        /* Crear asignatura a partir de numero de codigo y nombre*/
                        System.out.print("Ingrese el número de código de asignatura (debe ser numérico): ");
                        strcodigo = inFromUser.readLine();
                        Long codigo = 0L;
                        try {
                            codigo = Long.parseLong(strcodigo);
                        } catch (Exception e1) {

                        }
                        System.out.print("Ingrese el nombre de asignatura: ");
                        String nombre_asignatura = inFromUser.readLine();

                        Asignatura a = new Asignatura(codigo, nombre_asignatura);
                        MetodoPost.crearAsignatura(a);

                        break;

                    case 4:
                        /* Listar asignaturas existentes en la base de datos */
                        asignatura = MetodoGet.listarAsignatura();

                        if (asignatura == null) {
                            System.out.println("Lista de Asignaturas vacia");
                        } else {
                            MostrarAsignaturas(asignatura);

                        }

                        break;

                    case 5:
                        /* Asocia una persona con una asignatura */
                        System.out.print("Ingrese el número de cédula (debe ser numérico): ");
                        strcedula = inFromUser.readLine();
                        cedula = 0L;
                        try {
                            cedula = Long.parseLong(strcedula);
                        } catch (Exception e1) {

                        }
                        System.out.print("Ingrese el número de código de asignatura (debe ser numérico): ");
                        strcodigo = inFromUser.readLine();
                        codigo = 0L;
                        try {
                            codigo = Long.parseLong(strcodigo);
                        } catch (Exception e1) {

                        }
                        /* Buscar si existen el numero de cedula y el codigo de asignatura,
                            si no existen el sistema lanzara mensaje de error
                         */

                        p = MetodoGet.BuscarPersona(cedula);
                        asig = MetodoGet.BuscarAsignatura(codigo);
                        if (p == null || asig == null) {
                            System.out.println("Datos no coinciden. Verifique numero de cedula de la persona y el "
                                    + "codigo de asignatura");
                        } else {
                            Persona_Asignatura pa = new Persona_Asignatura();
                            pa.setCedulaPersona(cedula);
                            pa.setCodigoAsignatura(codigo);
                            MetodoPost.asociarPersona(pa);
                        }

                        break;

                    case 6:
                        /* Listar las asignaturas que una persona posee */
                        System.out.print("Ingrese el número de cédula (debe ser numérico): ");
                        strcedula = inFromUser.readLine();
                        cedula = 0L;
                        try {
                            cedula = Long.parseLong(strcedula);
                        } catch (Exception e1) {

                        }

                        /*
                        * Buscar si la persona existe en la base de datos 
                        * sino existe el sistema lanzara un error
                         */
                        p = MetodoGet.BuscarPersona(cedula);
                        if (p == null) {

                            System.out.println("Datos no coinciden. Verifique numero de cedula de la persona");
                        } else {
                            asignatura = MetodoGet.AsignaturaPersona(cedula);
                            if (asignatura == null) {
                                System.out.println("Lista de Asignaturas vacia");
                            } else {
                                MostrarAsignaturas(asignatura);
                            }
                        }

                        break;
                    case 7:
                        /* Listar las personas que existen en una asignatura*/
                        System.out.print("Ingrese el número de código de asignatura (debe ser numérico): ");
                        strcodigo = inFromUser.readLine();
                        codigo = 0L;
                        try {
                            codigo = Long.parseLong(strcodigo);
                        } catch (Exception e1) {

                        }
                        /*
                        * Busca si existe la asignatura en la base de datos
                        * sino lanza un mensaje de error
                         */
                        asig = MetodoGet.BuscarAsignatura(codigo);
                        if (asig == null) {
                            System.out.println("Datos no coinciden. Verifique el codigo de asignatura");
                        } else {
                            persona = MetodoGet.PersonaAsignatura(codigo);
                            if (persona == null) {
                                System.out.println("Lista de Personas vacia");
                            } else {
                                MostrarPersonas(persona);
                            }

                        }

                        break;

                    case 8:
                        salir = true;
                        break;
                    default:
                        System.out.println("Opcion no valida");
                        break;
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        } while (!salir);
    }

    /* Metodos de listar personas y asignaturas */
    public static void MostrarPersonas(List<Persona> p) {
        System.out.println("\t Cedula \t Nombre \t Apellido\n");
        for (int i = 0; i < p.size(); i++) {
            System.out.println("\t" + p.get(i).getCedula() + "\t\t" + p.get(i).getNombre()
                    + "\t\t" + p.get(i).getApellido());

        }
    }

    public static void MostrarAsignaturas(List<Asignatura> a) {
        System.out.println("\t Codigo \t Nombre\n");
        for (int i = 0; i < a.size(); i++) {
            System.out.println("\t" + a.get(i).getCodigo() + "\t\t" + a.get(i).getNombre());

        }
    }

}
