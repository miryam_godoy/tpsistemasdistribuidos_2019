package py.una.pol.cliente.metodos;

import static com.sun.corba.se.spi.presentation.rmi.StubAdapter.request;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import py.una.pol.cliente.model.Asignatura;
import py.una.pol.cliente.model.Persona;
/*
* Clientes del metodo GET
*/

public class MetodoGet {

    /*
    * Cliente para listar personas 
     */

    public static List<Persona> listarPersonas() throws ParseException {

        final String targetURL = "http://localhost:8080/personas/rest/personas";

        List<Persona> lista = null;

        try {

            URL restServiceURL = new URL(targetURL);

            HttpURLConnection httpConnection = (HttpURLConnection) restServiceURL.openConnection();
            httpConnection.setRequestMethod("GET");
            httpConnection.setRequestProperty("Accept", "application/json");

            if (httpConnection.getResponseCode() < 200 || httpConnection.getResponseCode() >= 300) {
                throw new RuntimeException("HTTP GET Request Failed with Error code : "
                        + httpConnection.getResponseCode());
            }

            BufferedReader responseBuffer = new BufferedReader(new InputStreamReader(
                    (httpConnection.getInputStream())));

            String output;

            while ((output = responseBuffer.readLine()) != null) {
                /* Pasar de JSON a Persona */

                JSONParser parser = new JSONParser();
                Object obj = parser.parse(output.trim());
                JSONArray json = (JSONArray) obj;
                lista = new ArrayList<Persona>();

                for (int i = 0; i < json.size(); ++i) {
                    JSONObject rec = (JSONObject) json.get(i);
                    Long cedula = (Long) rec.get("cedula");
                    Persona p = new Persona();
                    p.setCedula(cedula);
                    p.setNombre((String) rec.get("nombre"));
                    p.setApellido((String) rec.get("apellido"));
                    lista.add(p);

                }

            }

            httpConnection.disconnect();

        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
        return lista;
    }

    /*
    * Cliente par listar Asignaturas
     */
    public static List<Asignatura> listarAsignatura() throws ParseException {

        final String targetURL = "http://localhost:8080/personas/rest/asignaturas";
        List<Asignatura> lista = null;

        try {

            URL restServiceURL = new URL(targetURL);

            HttpURLConnection httpConnection = (HttpURLConnection) restServiceURL.openConnection();
            httpConnection.setRequestMethod("GET");
            httpConnection.setRequestProperty("Accept", "application/json");

            if (httpConnection.getResponseCode() < 200 || httpConnection.getResponseCode() >= 300) {
                throw new RuntimeException("HTTP GET Request Failed with Error code : "
                        + httpConnection.getResponseCode());
            }

            BufferedReader responseBuffer = new BufferedReader(new InputStreamReader(
                    (httpConnection.getInputStream())));

            String output;

            while ((output = responseBuffer.readLine()) != null) {
                /* Pasar de JSON a Asignatura */

                JSONParser parser = new JSONParser();
                Object obj = parser.parse(output.trim());
                JSONArray json = (JSONArray) obj;
                lista = new ArrayList<Asignatura>();

                for (int i = 0; i < json.size(); ++i) {
                    JSONObject rec = (JSONObject) json.get(i);
                    Long codigo = (Long) rec.get("codigo");
                    Asignatura a = new Asignatura();
                    a.setCodigo(codigo);
                    a.setNombre((String) rec.get("nombre"));
                    lista.add(a);

                }

            }

            httpConnection.disconnect();

        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
        return lista;

    }

    /*
    * Cliente para buscar una persona por su nro de cedula
     */

    public static Persona BuscarPersona(Long cedula_buscar) throws ParseException {

        final String targetURL = "http://localhost:8080/personas/rest/personas/" + cedula_buscar;

        Persona p = null;

        try {

            URL restServiceURL = new URL(targetURL);

            HttpURLConnection httpConnection = (HttpURLConnection) restServiceURL.openConnection();
            httpConnection.setRequestMethod("GET");
            httpConnection.setRequestProperty("Accept", "application/json");

            if (httpConnection.getResponseCode() < 200 || httpConnection.getResponseCode() >= 300) {
                throw new RuntimeException("HTTP GET Request Failed with Error code : "
                        + httpConnection.getResponseCode());
            }

            BufferedReader responseBuffer = new BufferedReader(new InputStreamReader(
                    (httpConnection.getInputStream())));

            String output;

            while ((output = responseBuffer.readLine()) != null) {
                /* Pasar de JSON a Persona */

                p = new Persona();
                JSONParser parser = new JSONParser();

                Object obj = parser.parse(output.trim());
                JSONObject jsonObject = (JSONObject) obj;

                Long cedula = (Long) jsonObject.get("cedula");
                p.setCedula(cedula);
                p.setNombre((String) jsonObject.get("nombre"));
                p.setApellido((String) jsonObject.get("apellido"));

            }

            httpConnection.disconnect();

        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
        return p;

    }

    /*
    * Cliente par buscar asignatura 
     */

    public static Asignatura BuscarAsignatura(Long codigo_buscar) throws ParseException {

        final String targetURL = "http://localhost:8080/personas/rest/asignaturas/" + codigo_buscar;

        Asignatura a = null;

        try {

            URL restServiceURL = new URL(targetURL);

            HttpURLConnection httpConnection = (HttpURLConnection) restServiceURL.openConnection();
            httpConnection.setRequestMethod("GET");
            httpConnection.setRequestProperty("Accept", "application/json");

            if (httpConnection.getResponseCode() < 200 || httpConnection.getResponseCode() >= 300) {
                throw new RuntimeException("HTTP GET Request Failed with Error code : "
                        + httpConnection.getResponseCode());
            }

            BufferedReader responseBuffer = new BufferedReader(new InputStreamReader(
                    (httpConnection.getInputStream())));

            String output;

            while ((output = responseBuffer.readLine()) != null) {
                /* Pasar de JSON a Persona */

                a = new Asignatura();
                JSONParser parser = new JSONParser();

                Object obj = parser.parse(output.trim());
                JSONObject jsonObject = (JSONObject) obj;

                Long codigo = (Long) jsonObject.get("codigo");
                a.setCodigo(codigo);
                a.setNombre((String) jsonObject.get("nombre"));

            }

            httpConnection.disconnect();

        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
        return a;

    }

    /*
    * Cliente para listar las asignaturas a la que esta inscripta una persona
     */
    public static List<Asignatura> AsignaturaPersona(Long cedula_buscar) throws ParseException {

        final String targetURL = "http://localhost:8080/personas/rest/persona_asignaturas/" + cedula_buscar;

        List<Asignatura> lista = null;

        try {

            URL restServiceURL = new URL(targetURL);

            HttpURLConnection httpConnection = (HttpURLConnection) restServiceURL.openConnection();
            httpConnection.setRequestMethod("GET");
            httpConnection.setRequestProperty("Accept", "application/json");

            if (httpConnection.getResponseCode() < 200 || httpConnection.getResponseCode() >= 300) {
                throw new RuntimeException("HTTP GET Request Failed with Error code : "
                        + httpConnection.getResponseCode());
            }

            BufferedReader responseBuffer = new BufferedReader(new InputStreamReader(
                    (httpConnection.getInputStream())));

            String output;

            while ((output = responseBuffer.readLine()) != null) {
                /* Pasar de JSON a Persona */

                lista = new ArrayList<Asignatura>();
                JSONParser parser = new JSONParser();
                Object obj = parser.parse(output.trim());
                JSONArray json = (JSONArray) obj;

                for (int i = 0; i < json.size(); ++i) {
                    JSONObject rec = (JSONObject) json.get(i);
                    Long codigo = (Long) rec.get("codigo");
                    Asignatura a = new Asignatura();
                    a.setCodigo(codigo);
                    a.setNombre((String) rec.get("nombre"));
                    lista.add(a);
                }

            }

            httpConnection.disconnect();

        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
        return lista;

    }

    /*
    * Cliente para listar las persona que estan inscrptar a una asignatura
     */
    public static List<Persona> PersonaAsignatura(Long codigo_buscar) throws ParseException {

        final String targetURL = "http://localhost:8080/personas/rest/persona_asignaturas/codigoAsignatura?codigoAsignatura=" + codigo_buscar;

        List<Persona> lista = null;

        try {

            URL restServiceURL = new URL(targetURL);

            HttpURLConnection httpConnection = (HttpURLConnection) restServiceURL.openConnection();
            httpConnection.setRequestMethod("GET");
            httpConnection.setRequestProperty("Accept", "application/json");

            if (httpConnection.getResponseCode() < 200 || httpConnection.getResponseCode() >= 300) {
                throw new RuntimeException("HTTP GET Request Failed with Error code : "
                        + httpConnection.getResponseCode());
            }

            BufferedReader responseBuffer = new BufferedReader(new InputStreamReader(
                    (httpConnection.getInputStream())));

            String output;

            while ((output = responseBuffer.readLine()) != null) {
                /* Pasar de JSON a Persona */

                lista = new ArrayList<Persona>();
                JSONParser parser = new JSONParser();
                Object obj = parser.parse(output.trim());
                JSONArray json = (JSONArray) obj;

                for (int i = 0; i < json.size(); ++i) {
                    JSONObject rec = (JSONObject) json.get(i);
                    Long cedula = (Long) rec.get("cedula");
                    Persona p = new Persona();
                    p.setCedula(cedula);
                    p.setNombre((String) rec.get("nombre"));
                    p.setApellido((String) rec.get("apellido"));
                    lista.add(p);

                }

            }

            httpConnection.disconnect();

        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
        return lista;

    }

}
