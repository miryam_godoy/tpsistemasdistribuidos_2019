package py.una.pol.cliente.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;



/**
 *
 * @author Acer
 */
@SuppressWarnings("serial")
@XmlRootElement
public class Persona_Asignatura implements Serializable {
    
   Long cedula_persona;
   Long codigo_asignatura;
  
    public Persona_Asignatura(){
               
		this.cedula_persona = 0L;
		this.codigo_asignatura = 0L;
	}

	public Persona_Asignatura(Long cedula_persona, Long codigo_asignatura ){
		this.cedula_persona = cedula_persona;
		this.codigo_asignatura = codigo_asignatura ;
		
		
		
	}
        
        public Long getCedulaPersona() {
		return cedula_persona;
	}

	public void setCedulaPersona(Long cedula_persona) {
		this.cedula_persona = cedula_persona;
	}

	public Long getCodigoAsignatura() {
		return codigo_asignatura;
	}

	public void setCodigoAsignatura(Long codigo_asignatura) {
		this.codigo_asignatura = codigo_asignatura;
	}
        
       
}
