**Universidad Nacional de Asunción**
**- Facultar Politécnica**

Sistemas Distribuidos 2019

Trabajo de Web service 

**Integrantes:**


* Miryam Godoy 


* Enzo Unzain  


**Usabilidad:**


1.  Crear las siguientes tablas en la base de datos  sd

```
CREATE TABLE public.persona
(
    cedula integer NOT NULL,
    nombre character varying(1000) COLLATE pg_catalog."default",
    apellido character varying(1000) COLLATE pg_catalog."default",
    CONSTRAINT persona_pkey PRIMARY KEY (cedula)
)
```


```
CREATE TABLE public.asignatura
(
    codigo integer NOT NULL,
    nombre character varying(1000) COLLATE pg_catalog."default",
    CONSTRAINT asignatura_pkey PRIMARY KEY (codigo)
)
```

```
CREATE TABLE public.persona_asignatura
(
    id bigint NOT NULL DEFAULT nextval('persona_asignatura_id_seq'::regclass),
    cedula_persona integer,
    codigo_asignatura integer,
    CONSTRAINT persona_asignatura_pkey PRIMARY KEY (id),
    CONSTRAINT persona_asignatura_cedula_persona_fkey FOREIGN KEY (cedula_persona)
        REFERENCES public.persona (cedula) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT persona_asignatura_codigo_asignatura_fkey FOREIGN KEY (codigo_asignatura)
        REFERENCES public.asignatura (codigo) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
```



2.  Deployar el servidor personas en WildFly o en JBoss 


3.  Ejecutar el cliente (MenuCliente.java) servidor 