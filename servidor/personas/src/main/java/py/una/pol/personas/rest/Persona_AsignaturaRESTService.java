package py.una.pol.personas.rest;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import py.una.pol.personas.model.Persona;
import py.una.pol.personas.model.Asignatura;
import py.una.pol.personas.model.Persona_Asignatura;
import py.una.pol.personas.service.Persona_AsignaturaService;

/**
 * JAX-RS Example
 * <p/>
 * Esta clase produce un servicio RESTful para leer y escribir contenido de personas
 */
@Path("/persona_asignaturas")
@RequestScoped
public class Persona_AsignaturaRESTService {
    @Inject
    private Logger log;

    @Inject
    Persona_AsignaturaService persona_asignaturaService;
    
    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<Persona_Asignatura> listar() {
        return persona_asignaturaService.seleccionar();
    }
    
    
    /*
    * Para mostrar las peronas inscriptas en una asignatura
    */
    
     @GET
    @Path("/codigoAsignatura")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Persona> obtenerPorIdQuery(@QueryParam("codigoAsignatura") long codigoAsignatura) {
        List<Persona> a = persona_asignaturaService.seleccionarPersonas(codigoAsignatura);
        if (a == null) {
        	log.info("obtenerPorId " + codigoAsignatura + " no encontrado.");
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        log.info("obtenerPorId " + codigoAsignatura + " encontrada: " );
        return a;
    }
    /*
    * Para listar asignaturas que posee una persona
    */
   
    @GET
    @Path("/{cedulaPersona:[0-9][0-9]*}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Asignatura> obtenerPorId(@PathParam("cedulaPersona") long cedulaPersona) {
        List<Asignatura> a = persona_asignaturaService.seleccionarAsignatura(cedulaPersona);
        if (a == null) {
        	log.info("obtenerPorId " + cedulaPersona + " no encontrado.");
                
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        
        log.info("obtenerPorId " + cedulaPersona + " encontrada: ");
        return a;
    }
    
    
    
    /**
     * Creates a new member from the values provided. Performs validation, and will return a JAX-RS response with either 200 ok,
     * or with a map of fields, and related errors.
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crear(Persona_Asignatura p) {

        Response.ResponseBuilder builder = null;

        try {
            persona_asignaturaService.crear(p);
            // Create an "ok" response
            
            //builder = Response.ok();
            builder = Response.status(201).entity("Persona asociada con Asignatura creada exitosamente");
            
        } catch (SQLException e) {
            // Handle the unique constrain violation
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("bd-error", e.getLocalizedMessage());
            builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }

        return builder.build();
    }

   @DELETE
    @Path("/{cedulaPersona}/{codigoAsignatura}")
   @Produces(MediaType.APPLICATION_JSON)
   public Response borrar(@PathParam("cedulaPersona") long cedulaPersona,
                           @PathParam("codigoAsignatura") long codigoAsignatura)
   {      
	   Response.ResponseBuilder builder = null;
	   try {
		   
		   if(persona_asignaturaService.seleccionarPersonas(cedulaPersona) == null ||
                        persona_asignaturaService.seleccionarAsignatura(codigoAsignatura) == null    ) {
			   
			   builder = Response.status(Response.Status.NOT_ACCEPTABLE).entity("Persona o Asignatura no existe.");
		   }else {
			   persona_asignaturaService.borrar(cedulaPersona, codigoAsignatura);
			   builder = Response.status(202).entity("Persona borrada de Asignatura exitosamente.");			   
		   }
		   

		   
	   } catch (SQLException e) {
           // Handle the unique constrain violation
           Map<String, String> responseObj = new HashMap<>();
           responseObj.put("bd-error", e.getLocalizedMessage());
           builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
       } catch (Exception e) {
           // Handle generic exceptions
           Map<String, String> responseObj = new HashMap<>();
           responseObj.put("error", e.getMessage());
           builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
       }
       return builder.build();
   }


       
}
