package py.una.pol.personas.service;

import javax.ejb.Stateless;
import javax.inject.Inject;

import py.una.pol.personas.dao.AsignaturaDAO;
import py.una.pol.personas.model.Asignatura;

import java.util.List;
import java.util.logging.Logger;

// The @Stateless annotation eliminates the need for manual transaction demarcation
@Stateless
public class AsignaturaService {
    @Inject
    private Logger log;

    @Inject
    private AsignaturaDAO dao;

    public void crear(Asignatura a) throws Exception {
        log.info("Creando Asignatura: " + a.getCodigo() + " " + a.getNombre());
        try {
        	dao.insertar(a);
        }catch(Exception e) {
        	log.severe("ERROR al crear asignatura: " + e.getLocalizedMessage() );
        	throw e;
        }
        log.info("Asignatura creada con éxito: " + a.getCodigo() + " " + a.getNombre() );
    }
    
    public List<Asignatura> seleccionar() {
    	return dao.seleccionar();
    }
    
    public Asignatura seleccionarPorCodigo(long codigo) {
    	return dao.seleccionarPorCodigo(codigo);
    }
    
    public Long borrar(Long codigo) throws Exception {
    	return dao.borrar(codigo);
    }
    
}






