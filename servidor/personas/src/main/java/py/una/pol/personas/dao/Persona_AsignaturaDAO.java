package py.una.pol.personas.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import py.una.pol.personas.model.Persona;
import py.una.pol.personas.model.Asignatura;
import py.una.pol.personas.model.Persona_Asignatura;

@Stateless

public class Persona_AsignaturaDAO {

    @Inject
    private Logger log;

    /**
     *
     * @param condiciones
     * @return
     */
    public List<Persona_Asignatura> seleccionar() {
        String query = "SELECT cedula_persona, codigo_asignatura FROM persona_asignatura ORDER BY cedula_persona";

        List<Persona_Asignatura> lista = new ArrayList<Persona_Asignatura>();

        Connection conn = null;
        try {
            conn = Bd.connect();
            ResultSet rs = conn.createStatement().executeQuery(query);

            while (rs.next()) {
                Persona_Asignatura p = new Persona_Asignatura();
                p.setCedulaPersona(rs.getLong(1));
                p.setCodigoAsignatura(rs.getLong(2));

                lista.add(p);
            }

        } catch (SQLException ex) {
            log.severe("Error en la seleccion: " + ex.getMessage());
        } finally {
            try {
                conn.close();
            } catch (Exception ef) {
                log.severe("No se pudo cerrar la conexion a BD: " + ef.getMessage());
            }
        }
        return lista;

    }

    /*
     * Listar las persona inscriptas en una asignatura
     */
    public List<Persona> listarPersonas(Long codigo_asignatura) {
        String query = "SELECT p.cedula, p.nombre, p.apellido FROM persona p "
                + "JOIN persona_asignatura asig ON  asig.cedula_persona = p.cedula "
                + "WHERE asig.codigo_asignatura = ?";

        List<Persona> lista = new ArrayList<Persona>();

        Connection conn = null;
        try {
            conn = Bd.connect();
            PreparedStatement sentencia = conn.prepareStatement(query);
            sentencia.setLong(1, codigo_asignatura);
            ResultSet rs = sentencia.executeQuery();

            while (rs.next()) {
                Persona p = new Persona();
                p.setCedula(rs.getLong(1));
                p.setNombre(rs.getString(2));
                p.setApellido(rs.getString(3));

                lista.add(p);
            }

        } catch (SQLException ex) {
            log.severe("Error en la seleccion: " + ex.getMessage());
        } finally {
            try {
                conn.close();
            } catch (Exception ef) {
                log.severe("No se pudo cerrar la conexion a BD: " + ef.getMessage());
            }
        }
        return lista;

    }

    /*
         Listar asignaturas que posee una persona
     */
    public List<Asignatura> listarAsignaturas(Long cedula_persona) {

        String query = "SELECT a.codigo, a.nombre FROM asignatura a "
                + "LEFT JOIN persona_asignatura asig ON  asig.codigo_asignatura = a.codigo "
                + "WHERE asig.cedula_persona = ?";

        List<Asignatura> lista = new ArrayList<Asignatura>();

        Connection conn = null;

        try {
            conn = Bd.connect();
            PreparedStatement sentencia = conn.prepareStatement(query);
            sentencia.setLong(1, cedula_persona);
            ResultSet rs = sentencia.executeQuery();
            //ResultSet rs = conn.createStatement().executeQuery(query);

            while (rs.next()) {
                Asignatura asignatura = new Asignatura();
                asignatura.setCodigo(rs.getLong(1));
                asignatura.setNombre(rs.getString(2));

                lista.add(asignatura);
            }

        } catch (SQLException ex) {
            log.severe("Error en la seleccion: " + ex.getMessage());
        } finally {
            try {
                conn.close();
            } catch (Exception ef) {
                log.severe("No se pudo cerrar la conexion a BD: " + ef.getMessage());
            }
        }
        return lista;

    }

    /*
         Asociar una persona con una asignatura
     */
    public long insertar(Persona_Asignatura pa) throws SQLException {

        String SQL = "INSERT INTO persona_asignatura(cedula_persona, codigo_asignatura) "
                + "VALUES(?,?)";

        long id = 0;
        Connection conn = null;

        try {
            conn = Bd.connect();
            PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            pstmt.setLong(1, pa.getCedulaPersona());
            pstmt.setLong(2, pa.getCodigoAsignatura());

            int affectedRows = pstmt.executeUpdate();
            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            }
        } catch (SQLException ex) {
            throw ex;
        } finally {
            try {
                conn.close();
            } catch (Exception ef) {
                log.severe("No se pudo cerrar la conexion a BD: " + ef.getMessage());
            }
        }

        return id;

    }

    /*
     Desasociar una persona de una asignatura
     */
    public long borrar(long cedula, long codigo) throws SQLException {

        String SQL = "DELETE FROM persona_asignatura WHERE cedula_persona = ?  and codigo_asignatura = ?";

        long id = 0;
        Connection conn = null;

        try {
            conn = Bd.connect();
            PreparedStatement pstmt = conn.prepareStatement(SQL);
            pstmt.setLong(1, cedula);
            pstmt.setLong(2, codigo);

            int affectedRows = pstmt.executeUpdate();
            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                    log.severe("Error en la eliminación: " + ex.getMessage());
                    throw ex;
                }
            }
        } catch (SQLException ex) {
            log.severe("Error en la eliminación: " + ex.getMessage());
            throw ex;
        } finally {
            try {
                conn.close();
            } catch (Exception ef) {
                log.severe("No se pudo cerrar la conexion a BD: " + ef.getMessage());
                throw ef;
            }
        }
        return id;
    }

}
