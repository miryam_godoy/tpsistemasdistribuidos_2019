/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.personas.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;


@SuppressWarnings("serial")
@XmlRootElement

/**
 *
 * @author Acer
 */
public class Asignatura implements Serializable{
    Long codigo;
    String nombre;
    
    public Asignatura(){
        this.codigo = 0L;
        this.nombre = "";
        
    }
     
    public Asignatura(Long acodigo, String anombre){
		this.codigo = acodigo;
		this.nombre = anombre;
		
		
	}
    
    public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	

    
	   
}

	