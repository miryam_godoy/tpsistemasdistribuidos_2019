package py.una.pol.personas.service;

import javax.ejb.Stateless;
import javax.inject.Inject;

import py.una.pol.personas.dao.Persona_AsignaturaDAO;
import py.una.pol.personas.model.Persona_Asignatura;
import py.una.pol.personas.model.Asignatura;
import py.una.pol.personas.model.Persona;

import java.util.List;
import java.util.logging.Logger;

// The @Stateless annotation eliminates the need for manual transaction demarcation
@Stateless
public class Persona_AsignaturaService {
    
    @Inject
    private Logger log;

    @Inject
    private Persona_AsignaturaDAO dao;

    public void crear(Persona_Asignatura p) throws Exception {
        log.info("Creando Persona_Asignatura: " + p.getCedulaPersona() + " " + p.getCodigoAsignatura());
        try {
        	dao.insertar(p);
        }catch(Exception e) {
        	log.severe("ERROR al crear persona_asignatura: " + e.getLocalizedMessage() );
        	throw e;
        }
        log.info("Persona_Asignatura creada con éxito: " + p.getCedulaPersona() + " " + p.getCodigoAsignatura() );
    }
    /*
    * Para listar las persona inscriptas en una asignatura
    */
    public List<Persona> seleccionarPersonas(long codigo) {
    	return dao.listarPersonas(codigo);
    }
    public List<Persona_Asignatura> seleccionar() {
    	return dao.seleccionar();
    }
    /*
    * Para listar las asignaturas de una persona
    */
    public List<Asignatura> seleccionarAsignatura(long cedula) {
    	return dao.listarAsignaturas(cedula);
    }
    
    public long borrar(long cedula, long codigo) throws Exception {
    	return dao.borrar(cedula, codigo);
    }
    
}
